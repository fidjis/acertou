import 'package:flutter/material.dart';

class PaletteCustomDialog extends StatefulWidget {

  final Function(Color newColor) callbackSetNewColor;

  PaletteCustomDialog({this.callbackSetNewColor});

  @override
  _PaletteCustomDialogState createState() => _PaletteCustomDialogState();
}

class _PaletteCustomDialogState extends State<PaletteCustomDialog> {
  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
      child: Container(
        height: 200,
        child: Padding(
          padding: const EdgeInsets.all(12.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text('Escolha uma cor!'),
              SizedBox(height: 25,),
              Row(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[ //primeira linha - criando as cores da paleta
                  buildColorOption(colorOption: Colors.red),
                  buildColorOption(colorOption: Colors.blue),
                  buildColorOption(colorOption: Colors.yellow),
                ],
              ),
              Row(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[//segunda linha - criando as cores da paleta
                  buildColorOption(colorOption: Colors.orange),
                  buildColorOption(colorOption: Colors.green),
                  buildColorOption(colorOption: Colors.black),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  //responsavel por construir cada opcao de cor da paleta
  buildColorOption({@required Color colorOption}){
    return GestureDetector(
      child: Card(color: colorOption, child: Container(height: 45, width: 45,),),
      onTap: (){
        Navigator.of(context).pop(); //fechar dialog ao selecionar cor
        widget.callbackSetNewColor(colorOption); //acionando o callback com a cor que foi selecionada
      },
    );
  }
}