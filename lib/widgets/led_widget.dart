import 'package:acertou/widgets/led_widget_components/led_digit_widget.dart';
import 'package:flutter/material.dart';

/*
* Responsavel por criar o widget que representa o painel de LEDs/seguimentos,
* consiste em um conjunto de 3 digitos (LedDigitWidget)
* O numero que recebe como paramentro sera exibido (ate 3 digitos)
*/
class LedPanelWidget extends StatefulWidget {
  final int number;
  final Color ledColor;

  LedPanelWidget({@required this.number, @required this.ledColor});

  @override
  LedPanelWidgetState createState() =>LedPanelWidgetState();
}

class LedPanelWidgetState extends State<LedPanelWidget> {

  Color _ledColor; //cor do led

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(5),
      height: 120, //tamanho fixo para melhor se enquadrar no layout
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: getDigitsPanel(widget.number),
      ),
    );
  }

  /*
  * Primeiro deve verificar quantos digitos tem no numero recebido,
  * pois caso seja menos que 3, deve-se apangar led que ficaria sem utilidade (LedDigitWidget)
  * O que faz com que o Led desaparaca e o fato de passar para o contruto um valor nulo (vide NumberLibrary para maiores esclarecimentos)
  */
  getDigitsPanel(int number){
    _ledColor = widget.ledColor;

    //converte o digito em um array para verificacao e processamento de cada valor
    List<String> numberString = number.toString().split("");
    switch (numberString.length) {
      case 3: //caso tenha 3 digitos retorna essa modelo de painel
        return [ 
          LedDigitWidget(number: int.parse(numberString[0]), ledColor: _ledColor,),
          LedDigitWidget(number: int.parse(numberString[1]), ledColor: _ledColor,),
          LedDigitWidget(number: int.parse(numberString[2]), ledColor: _ledColor,),
        ];
        break;
      case 2: //caso tenha 2 digitos retorna essa modelo de painel
        return [ 
          LedDigitWidget(number: int.parse(numberString[0]), ledColor: _ledColor,),
          LedDigitWidget(number: int.parse(numberString[1]), ledColor: _ledColor,),
          LedDigitWidget(number: null, ledColor: _ledColor,), //null deixa os leds apagados
        ]; 
        break;
      case 1: //ultima opcao de modelo (para apeas 1 digito)
        return [ 
          LedDigitWidget(number: int.parse(numberString[0]), ledColor: _ledColor,),
          LedDigitWidget(number: null, ledColor: _ledColor,),
          LedDigitWidget(number: null, ledColor: _ledColor,), //null deixa os leds apagados
        ];
        break;
      default:
        break;
    }
  }
}