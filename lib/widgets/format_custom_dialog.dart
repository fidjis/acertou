import 'package:flutter/material.dart';

class FormatCustomDialog extends StatefulWidget {
  @override
  _FormatCustomDialogState createState() => _FormatCustomDialogState();
}

class _FormatCustomDialogState extends State<FormatCustomDialog> {
  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
      child: Container(
        height: 200,
        child: Padding(
          padding: const EdgeInsets.all(12.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text('Tamanho do LED!'),
              SizedBox(height: 40,),
              Text(
                'Em construção - Aguarde as proximas atualizações!',
                textAlign: TextAlign.center,
              ),
              SizedBox(height: 35,),
              GestureDetector(
                child: Text(
                  'Fechar',
                  style: TextStyle(color: Colors.red),
                ),
                onTap: (){
                  Navigator.of(context).pop(); //fechando 
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}