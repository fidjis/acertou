import 'package:flutter/material.dart';

/*
* Responsavel por criar cada ponto/linha dos seguimentos,
* Para utilizar bastar crir o widget e passar dois parametro
*   - lineEnabled = se a linha sera visivel
*   - isHorizontal = se a linha deve ser na vertical ou horizontal
*/
class LineWidget extends StatefulWidget {
  final bool lineEnabled; //se a linha sera visivel
  final bool isHorizontal; //se a linha deve ser na vertical ou horizontal
  final Color ledColor; //cor do led

  LineWidget({
    @required this.lineEnabled,
    @required this.isHorizontal,
    @required this.ledColor,
  });

  @override
  LineWidgetState createState() => LineWidgetState();
}

class LineWidgetState extends State<LineWidget> {

  @override
  Widget build(BuildContext context) {
    //controle de qual widget sera retornado (vertical ou horizontal)
    return widget.isHorizontal ? buildHorizontalLine() : buildVerticalLine();
  }

  //Construtor da layout da linha na Horizontal
  buildHorizontalLine() {
    return AnimatedOpacity( //AnimatedOpacity para mudanca entra aceso/apagado ser mais suave
      opacity: widget.lineEnabled ? 1 : 0,
      duration: const Duration(milliseconds: 100),
      child: Container(
        height: 10, 
        width: 40, 
        color: widget.ledColor,
        margin: EdgeInsets.only(right: 10, left: 10),
      ),
    );
  }

  //Construtor da layout da linha na Vertical
  buildVerticalLine() {
    return AnimatedOpacity( //AnimatedOpacity para mudanca entra aceso/apagado ser mais suave
      opacity: widget.lineEnabled ? 1 : 0,
      duration: const Duration(milliseconds: 200),
      child: Container(
        height: 40, 
        width: 10, 
        color: widget.ledColor,
      )
    );
  }
}