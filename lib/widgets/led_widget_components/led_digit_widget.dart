import 'package:acertou/utils/number_library.dart';
import 'package:acertou/widgets/led_widget_components/line_widget.dart';
import 'package:flutter/material.dart';

/*
* Responsavel por criar o widget onde sera possivel ver 1 digito,
* Recebe apenas 1 numero como paramentro, que e o que sera exibido (penas 1 digitos)
* consiste em um conjunto de 7 linhas (LineWidget)
*/
class LedDigitWidget extends StatefulWidget {

  final int number; //numero de 1 digito que aparecera no LED 
  final Color ledColor; //cor do led
  
  LedDigitWidget({@required this.number, @required this.ledColor});

  @override
  _LedDigitWidgetState createState() => _LedDigitWidgetState();
}

class _LedDigitWidgetState extends State<LedDigitWidget> {

  bool _visibility; //controla se o widget num todo sera visto ou nao
  Color _ledColor; //cor do led
  
  /*
  * Variavel _number Recebe os valores do getNumber  
  * metodo da classe NumberLibrary, que foi criada para decodificar quais linhas (LineWidget)
  * precisam estar acesas para que o digito seja formado
  */
  List<bool> _number; 

  @override
  Widget build(BuildContext context) {
    /*
    * Variaveis sendo setadas durante buil devido falha em  
    * mudancas de status durante ciclo de vida das classes ao qual esse 
    * widget serve (LedPanelWidget - HomeScreen)
    */

    //caso as valores iniciais ou recebidos posteriormente sejam nulos,
    //conforme na NumberLibrary significa que as linhas ficarao todasapagadas 
    //(vide NumberLibrary para maiores esclarecimentos)
    _visibility = widget.number != null; 
    _number = NumberLibrary.getNumber(widget.number);
    _ledColor = widget.ledColor;

    //As sequencias a baixo montam as linhas na ordem para formar a estrutura do digito
    return Visibility(
      visible: _visibility,
      child: Padding(
        padding: const EdgeInsets.all(5.0),
        child: Column( 
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            LineWidget(isHorizontal: true, lineEnabled: _number[0], ledColor: _ledColor,), //linha horizontal do top
            Row(
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                LineWidget(isHorizontal: false, lineEnabled: _number[1], ledColor: _ledColor,), //primeira linha vertical a esqueda
                Container(width: 40,),
                LineWidget(isHorizontal: false, lineEnabled: _number[2], ledColor: _ledColor,), //primeira linha vertical a direita
              ],
            ),
            LineWidget(isHorizontal: true, lineEnabled: _number[3], ledColor: _ledColor,), // linha horizontal central
            Row(
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                LineWidget(isHorizontal: false, lineEnabled: _number[4], ledColor: _ledColor,), //segunda linha vertical a esqueda
                Container(width: 40,),
                LineWidget(isHorizontal: false, lineEnabled: _number[5], ledColor: _ledColor,), //primeira linha vertical a direita
              ],
            ),
            LineWidget(isHorizontal: true, lineEnabled: _number[6], ledColor: _ledColor,), //linha horizontal da base 
          ],
        ),
      ),
    );
  }
}