import 'dart:convert';
import 'package:acertou/models/post.dart';
import 'package:acertou/widgets/format_custom_dialog.dart';
import 'package:acertou/widgets/led_widget.dart';
import 'package:acertou/widgets/palette_custom_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;

/*
* Tela principal do APP, (Unica Tela)
* Apresenta toda a estrutura do app, utilizando Widgets nativos e o Widget personlizado LedPanelWidget,
* para implemtnar todas as funcionalidades,
* O app consiste em receber 1 numero (de 1 a 300) do usuario, e comparar com um outro numero,
* recebido via WEB. A requisicao WEB e feita no metodo requestPost() e o app so e contruido para o usuario,
* apos receber o resultado do primeiro request,
* Os erros e os valores digitados pelo usuario sao mostrado no wiget LedPanelWidget, caso ele erre ou acerte,
* em volta do widget sao apresentados textos que tem a funcao de serem dicas, indicadores de status, tentativas, etc
*/
class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {

  Post postData; //armazenara tudo que for recebido viw WEB
  String message; //mensagem de erro, acerto, dica, etc
  int valueDlisplay; //valor escolhido pelo usuario ou valor do erro, sera sempre mostrado no Led Painel
  bool newMatch ; //necessario quando o ususario acertar o numero ou quando a requisicao recebe um erro
  int tentativas; //para interacoes com o usuario
  Color ledColor; //cor da font do LED
  final _formKey = new GlobalKey<FormState>(); //para fazer a validacao do valor digitado pelo usuario
  TextEditingController _valueChoiceController = new TextEditingController(); //para pegar o valor digitado pelo usuario

  @override
  void initState() {
    ledColor = Colors.red;
    message = "";
    valueDlisplay = 0;
    newMatch = false;
    tentativas = 0;
    requestPost(); 
  }

  //responsavel por buscar  o numero que o usuario devera acertar
  requestPost() async {
    //link da API Web
    final response = await http.get('https://us-central1-ss-devops.cloudfunctions.net/rand?min=1&max=300');
    //logica para verificar se temos um resultado valido
    if (response.statusCode == 200) {
      //como foi retornado algum valor, a variavel sera atualizada para que o corpo do app seja construido
      setState(() => postData = Post.fromJson(json.decode(response.body)));
      print(postData.value);
      
      //Comecando com a validacao do que foi recebido
      if(postData.value == null) //caso postData.value nao tenha valor significa que foi um erro recebido no json
        setState(() { //setando as informacoes do erro para ser apresentado ao usuario
          message = "ERRO";
          newMatch = true; //abilidanto possibilidade de tentar nova partida
          valueDlisplay = int.parse(postData.error);
          tentativas = 0;
        }); 
        
    } else { //setando as informacoes do erro para ser apresentado ao usuario
      setState(() {
        valueDlisplay = response.statusCode;
        postData = new Post(value: null, error: valueDlisplay.toString());
        message = "ERRO";
        tentativas = 0;
        newMatch = true;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Theme.of(context).primaryColor,
          title: Row( //devido a necessidade de centralizar os IconButton foram implemntados no aqui
            children: <Widget>[
              Expanded(flex: 1, child: Container()),
              Expanded(flex: 1, child: Text('ACERTOU?\n🙈', textAlign: TextAlign.center,)),
              Expanded(flex: 1, child: Row(mainAxisAlignment: MainAxisAlignment.end, children: <Widget>[
                IconButton( //botao de selecionar o tamanho do texto do painel 
                  icon: Image.asset("assets/imgs/baseline_format_size_black_18dp.png", 
                  color: Colors.white,), 
                  onPressed: () {
                    showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return FormatCustomDialog(); //Dialogo personalizado para escolher font do led
                      }
                    );
                  }
                ),
                IconButton( //botao de selecionar a cor do texto do painel
                  icon: Image.asset("assets/imgs/baseline_palette_black_18dp.png",
                  color: Colors.white,), 
                  onPressed: () {
                    showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return PaletteCustomDialog( //Dialogo personalizado para escolher cor
                          callbackSetNewColor: (newColor){ //passando como callback a funcao que setara a nova cor
                            setState(()=> ledColor = newColor);
                          },
                        );
                    });
                  }
                ),
              ],)),
            ],
          ),
        ),
        body: buildBody(),
      ),
    );
  }

  //Corpo do app
  buildBody() {
    return Padding(
        padding: const EdgeInsets.all(5.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            if(postData == null) ...[ //loading enquanto busca os dados via REST
              Center(child: CircularProgressIndicator()),
            ] 
            else ...[ //Layout apos receber os dados
              Expanded(
                flex: 8, 
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Expanded(
                      flex: 6,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          Text(
                            '$message',
                            style: TextStyle(
                              color: ledColor,
                              fontSize: 17,
                            ),
                          ),
                          Center( //painel onde os valores e erros serao exibidos
                            child: LedPanelWidget(
                              number: valueDlisplay, 
                              ledColor: ledColor,
                            ),
                          ), 
                          Text(
                            tentativas == 0 ? "Tente Acertar!" : "Tentativas: $tentativas", //caso 0, o layout fica melhor com esse campo escondido
                            style: TextStyle(
                              fontSize: 15,
                              color: ledColor,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Expanded(
                      flex: 4,
                      child: newMatchButton(), //widget do botao para nova partida
                    ),
                  ],
                ),
              ),
              Expanded(
                flex: 2, 
                child: Padding(
                  padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                  child: formWidget(), //widget do formulario que o usuario incere o palpite 
                ),
              )
            ]
          ],
        ),
      );
  }

  //botao que aparece para iniciar nova partida
  newMatchButton() {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        AnimatedOpacity(
          //A animacao fica melhor quando desaparece mais rapido
          duration: newMatch ? const Duration(milliseconds: 1300) : const Duration(milliseconds: 400),
          opacity: newMatch ? 1 : 0,
          child: Padding(
            padding: const EdgeInsets.only(top: 8.0),
            child: FloatingActionButton.extended(
              backgroundColor: Theme.of(context).primaryColor,
              label: Text("Nova Partida"),
              onPressed: (){
                if(newMatch){ //widget funciona apenas quando visivel
                  setState((){ //setando variaveis para a nova partida
                    newMatch = false;
                    tentativas = 0;
                    message = "";
                    valueDlisplay = 0;
                  }); //escondendo botao
                  requestPost(); //buscando dado para nova partida
                }
              }
            ),
          ),
        ),
      ],
    );
  }

  //widget do formulario que o usuario incere o palpite 
  formWidget() {
    return Row(
      children: <Widget>[
        choiceInput(), //Widget onde usuario digita o valor que escolheu
        FloatingActionButton( //botao para o envio/processamento do palpite
          mini: true,
          child: Icon(Icons.keyboard_arrow_right, size: 40,),
          backgroundColor: newMatch ? Theme.of(context).secondaryHeaderColor : Theme.of(context).primaryColor,
          onPressed: newMatch ? null : (){ //enquanto nao habilitar uma nova partia, o botao ira funcionar
            if(_formKey.currentState.validate()){
              //FocusScope.of(context).requestFocus(new FocusNode()); //tirar o foco(esconder teclado)
              _formKey.currentState.save();
              _valueChoiceController.text = ""; //limpando apos salvar

              //Logica referente a comparacao dos dados disponiveis com o recebido
              if(valueDlisplay == postData?.value) //caso tenha acertado a numero
                setState(() {
                  newMatch = true;
                  message = "ACERTOU!";
                }); 
              else if(valueDlisplay > postData.value) //caso o palpite seja maior que o numero oculto
                setState(() => message = "É MENOR!"); 
              else if(valueDlisplay < postData.value) //caso o palpite seja menor que o numero oculto
                setState(() => message = "É MAIOR!");

              setState(() => tentativas++); //contando quantas vezes o usuario tentou
            }
          },
        ),
      ],
    );
  }

  //Widget onde usuario digita o valor que escolheu
  choiceInput() {
    return Expanded(
      child: Form(
        key: _formKey,
        child: TextFormField(
          controller: _valueChoiceController,
          onSaved: (newValue){ //salvando o valor para aparecer no display
            setState(() {
              valueDlisplay = int.parse(newValue);
            });
          },
          validator: (newValue){  //logica para validacao do numero digitato (deve ser entre 1 e 300)
            if (newValue.isNotEmpty) {
              int newValueInt = int.parse(newValue); 
              if(newValueInt <= 300 && newValueInt >= 1)
                return null;
              else{
                _valueChoiceController.text = "";
                return 'Escolha entre 1 e 300';
              }
            } else 
              return 'Escolha entre 1 e 300';
          },
          decoration: new InputDecoration(
            hintText: "Digite o palpite",
            hintStyle: TextStyle(color: Theme.of(context).primaryColor),
            enabledBorder: UnderlineInputBorder(borderSide: BorderSide(color: Theme.of(context).primaryColor)),  
            focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: Theme.of(context).primaryColor)),
          ),
          keyboardType: TextInputType.number,
          inputFormatters: [
            WhitelistingTextInputFormatter.digitsOnly, //apenas numeros
            LengthLimitingTextInputFormatter(3), //apenas 3 digitos
          ],
        ),
      ),
    );
  }
}
