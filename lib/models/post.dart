//classe modelo para os dados recebidos via WEB
class Post {
  final int value; //valor que vira no json
  final String error; //erro que vira no json

  Post({this.value, this.error});

  factory Post.fromJson(Map<String, dynamic> json) {
    //tentando buscar os valores, caso nao tenha no json, nao tem problemas, pois se uma das variaveis
    //for nula, isso desencadeia um processo diferente (Verificar HOMESCREEN)
    return Post(
      value: json['value'], //numero que o usario deve acertar
      error: json['StatusCode'], //erro
    );
  }
}
