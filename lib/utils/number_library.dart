//classe responsavel por fornecessar a cadeia de boleans para cada linha
//que devem estar ativas para formar o numero no displey de led

class NumberLibrary{
  static List<bool> getNumber(int number){ //null e o parametro que vai apagar o led
    switch (number) {
      //abaixo cada numero retona sua proprio representacao
      //caso receba com paramentro o valor null, entao subentende que a sequencia 
      //de retorno e para apagar todas as linhas 
      case 0:
        return [true,true,true,false,true,true,true];
        break;
      case 1:
        return [false,false,true,false,false,true,false];
        break;
      case 2:
        return [true,false,true,true,true,false,true];
        break;
      case 3:
        return [true,false,true,true,false,true,true];
        break;
      case 4:
        return [false,true,true,true,false,true,false];
        break;
      case 5:
        return [true,true,false,true,false,true,true];
        break;
      case 6:
        return [true,true,false,true,true,true,true];
        break;
      case 7:
        return [true,false,true,false,false,true,false];
        break;
      case 8:
        return [true,true,true,true,true,true,true];
        break;
      case 9:
        return [true,true,true,true,false,true,false];
        break;
      default:
        return [false,false,false,false,false,false,false]; //por defaut apaga todo o led
        break;
    }
  }
}